\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key d \major

		d' 2 b 8 cis' d' cis' ~  |
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a b a  |
%% 5
		d' 2 b 8 cis' d' cis' ~  |
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a fis ( g )  |
		fis 1  |
%% 10
		R1  |
		d' 2 b 8 cis' d' cis' ~  |
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a b a  |
%% 15
		d' 2 b 8 cis' d' cis' ~  |
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a fis ( g )  |
		fis 1  |
%% 20
		R1*2  |
		r2 r4 r8 a  |
		b 4 b b b  |
		b 2 a 4 r  |
%% 25
		R1  |
		r2 r4 r8 a  |
		b 4 b b b  |
		b 1 (  |
		a 2 ) r  |
%% 30
		d' 2 b 8 cis' d' cis' ~  |
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a b a  |
		d' 2 b 8 cis' d' cis' ~  |
%% 35
		cis' 4 r cis' 8 b a b ~  |
		b 4 r b 8 a fis b ~  |
		b 4 a fis ( g )  |
		fis 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "tenor" {
		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		% Ho -- san -- na en el cie -- lo,
		"...ho" -- san -- na en el cie -- lo.
		% Ben -- di -- to el que vie -- ne
		"...en" nom -- bre del Se -- ñor. __

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.
	}
>>
