\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		a' 2 fis' 8 g' a' a' ~  |
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' e'  |
%% 5
		a' 2 fis' 8 g' a' a' ~  |
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' ( e' )  |
		d' 1  |
%% 10
		R1  |
		a' 2 fis' 8 g' a' a' ~  |
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' e'  |
%% 15
		a' 2 fis' 8 g' a' a' ~  |
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' ( e' )  |
		d' 1  |
%% 20
		r2. r8 a  |
		d' 4 d' fis' g'  |
		a' 2 fis' 4 r8 a'  |
		g' 4 fis' e' d'  |
		fis' 2 e' 4 r8 a  |
%% 25
		d' 4 d' fis' g'  |
		a' 2 fis' 4 r8 a'  |
		g' 4 fis' e' d'  |
		a' 1 ~  |
		a' 2 r  |
%% 30
		a' 2 fis' 8 g' a' a' ~  |
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' e'  |
		a' 2 fis' 8 g' a' a' ~  |
%% 35
		a' 4 r a' 8 g' fis' g' ~  |
		g' 4 r g' 8 fis' d' g' ~  |
		g' 4 fis' d' ( e' )  |
		d' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "soprano" {
		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		Ho -- san -- na en el cie -- lo,
		ho -- san -- na en el cie -- lo.
		Ben -- di -- to el que vie -- ne
		en nom -- bre del Se -- ñor. __

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.
	}
>>
