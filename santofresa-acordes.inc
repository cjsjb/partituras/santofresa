\context ChordNames
	\chords {
	\set chordChanges = ##t

	% saaanto es el sennooor...
	r1*9
	a1

	% saaanto es el sennooor...
	d1 fis1:m g1 a1
	d1 fis1:m g1 a1
	d1 a1

	% hosanna en el cieeelo...
	d1 fis1:m g1 a1
	d1 fis1:m g1 a1:sus4 a1

	% saaanto es el sennooor...
	d1 fis1:m g1 a1
	d1 fis1:m g1 a1
	d1 d1
	}
