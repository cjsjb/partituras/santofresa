\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		fis' 2 d' 8 e' fis' fis' ~  |
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b a  |
%% 5
		fis' 2 d' 8 e' fis' fis' ~  |
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b ( cis' )  |
		a 1  |
%% 10
		R1  |
		fis' 2 d' 8 e' fis' fis' ~  |
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b a  |
%% 15
		fis' 2 d' 8 e' fis' fis' ~  |
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b ( cis' )  |
		a 1  |
%% 20
		r2. r8 a  |
		d' 4 d' fis' g'  |
		fis' 2 cis' 4 r8 cis'  |
		d' 4 d' d' d'  |
		d' 2 cis' 4 r8 a  |
%% 25
		d' 4 d' fis' g'  |
		fis' 2 cis' 4 r8 cis'  |
		d' 4 d' d' d'  |
		d' 1 (  |
		cis' 2 ) r  |
%% 30
		fis' 2 d' 8 e' fis' fis' ~  |
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b a  |
		fis' 2 d' 8 e' fis' fis' ~  |
%% 35
		fis' 4 r cis' 8 cis' cis' d' ~  |
		d' 4 r d' 8 d' d' d' ~  |
		d' 4 cis' b ( cis' )  |
		a 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzo" {
		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.

		Ho -- san -- na en el cie -- lo,
		ho -- san -- na en el cie -- lo.
		Ben -- di -- to el que vie -- ne
		en nom -- bre del Se -- ñor. __

		San -- "to es" el Se -- ñor, __ Dios cre -- a -- dor __
		del u -- ni -- ver -- "so en" -- te -- ro,
		y lle -- nos es -- tán __ de su bon -- dad
		to -- da la tie -- "rra y" cie __ lo.
	}
>>
